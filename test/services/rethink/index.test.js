'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('rethink service', function() {
  it('registered the rethinks service', () => {
    assert.ok(app.service('rethinks'));
  });
});
